/**
* Paint.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    title: {
      type: 'string'
    },

    description: {
      type: 'string'
    },

    owner: {
      model: 'user'
    },
    
    // An array of video ids representing the manual (human) ordering of videos. 
    
    ratings: {
      collection: 'rating',
      via: 'byPaint'
    },

    imgSrc:{
      type:'string',
    },
    
    width:{
      type: 'Integer'
    },
    
    height:{
      type: 'Integer'
    },
    
    price:{
      type: 'Integer'
    },
    
    reviews: {
      collection: 'review',
      via: 'paint'
    },
  
  }
};

