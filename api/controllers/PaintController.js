/**
 * PaintController
 *
 * @description :: Server-side logic for managing paint
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  searchPaints: function(req, res) {

    Paint.count().exec(function(err, found){
      if (err) return res.negotiate(err);
      if (!found) return res.notFound();

      Paint.find({
        or : [
          {
            title: {
              'contains': req.param('searchCriteria')
            },
          },
          {
            description: {
              'contains': req.param('searchCriteria')
            }
          }
        ],
        limit: 10,
        skip: req.param('skip')
      })
      .populate('owner')
      .populate('ratings')
      .exec(function(err, paints){

        // Iterate through paints to format the owner and created attributes
        _.each(paints, function(paint){

          paint.owner = paint.owner.username;
          paint.created = DatetimeService.getTimeAgo({date: paint.createdAt});

          // Determine the total seconds for all videos and each video
          var totalSeconds = 0;
        });

        return res.json({
          options: {
            totalPaints: found,
            updatedPaints: paints
          }
        });
      });
    });
  },

  browsePaints: function(req, res) {
    
    Paint.count().exec(function (err, numberOfPaints){
      if (err) return res.negotiate(err);
      if (!numberOfPaints) return res.notFound();

      Paint.find({
        limit: 10,
        skip: req.param('skip')
      })
      .populate('owner')
      .populate('ratings')
      .exec(function(err, foundPaints){

        _.each(foundPaints, function(paint){

          paint.owner = paint.owner.username;
          paint.created = DatetimeService.getTimeAgo({date: paint.createdAt});

          var totalSeconds = 0;
      
        });

        return res.json({
          options: {
            totalPaints: numberOfPaints,
            updatedPaints: foundPaints
          }
        });
      });
    });
  },

  myRating: function(req, res) {

    return res.json({
      myRating: null
    });
  },

  ratePaint: function(req, res) {

    // Find the currently authenticated user
    User.findOne({
      id: req.session.userId
    })
    .exec(function(err, currentUser){
      if (err) return res.negotiate(err);
      if (!currentUser) return res.notFound();

      // Find the paint being rated
      Paint.findOne({
        id: +req.param('id')
      })
      .populate('owner')
      .exec(function(err, foundPaint){
        if (err) return res.negotiate(err);
        if (!foundPaint) return res.notFound();

        // Assure that the owner of the paint cannot rate their own paint.
        // Note that this is a back-up to the front-end which already prevents the UI from being displayed.
        
        if (currentUser.id === foundPaint.owner.id) {
          return res.forbidden();
        }

        // Find the rating, if any, of the paint from the currently logged in user.
        Rating.findOne({
          byUser: currentUser.id,
          byPaint: foundPaint.id
        }).exec(function(err, foundRating){
          if (err) return res.negotiate(err);

          // If the currently authenticated user-agent (user) has previously rated this 
          // paint update it with the new rating.
          if (foundRating) {

            Rating.update({
              id: foundRating.id
            }).set({
              stars: req.param('stars')
            }).exec(function(err, updatedRating){
              if (err) return res.negotiate(err);
              if (!updatedRating) return res.notFound();

              // Re-find the paint whose being rated to get the latest
              Paint.findOne({
                id: req.param('id')
              })
              .populate('ratings')
              .exec(function(err, foundPaintAfterUpdate){
                if (err) return res.negotiate(err);
                if (!foundPaintAfterUpdate) return res.notFound();

                return res.json({
                  averageRating: MathService.calculateAverage({ratings: foundPaintAfterUpdate.ratings})
                });
              });
            });

          // If the currently authenticated user-agent (user) has not already rated this
          // paint create it with the new rating.
          } else {
            Rating.create({
              stars: req.param('stars'),
              byUser: currentUser.id,
              byPaint: foundPaint.id
            }).exec(function(err, createdRating){
              if (err) return res.negotiate(err);
              if (!createdRating) return res.notFound();

              // Re-Find the paint whose being rated to get the latest
              Paint.findOne({
                id: req.param('id')
              })
              .populate('ratings')
              .exec(function(err, foundPaintAfterUpdate){
                if (err) return res.negotiate(err);
                if (!foundPaintAfterUpdate) return res.notFound();

                return res.json({
                  averageRating: MathService.calculateAverage({ratings: foundPaintAfterUpdate.ratings})
                });
              });
            });
          }
        });
      });
    });
  },

  createPaint: function(req, res) {
    


    /*
     __   __    _ _    _      _   _          
     \ \ / /_ _| (_)__| |__ _| |_(_)___ _ _  
      \ V / _` | | / _` / _` |  _| / _ \ ' \ 
       \_/\__,_|_|_\__,_\__,_|\__|_\___/_||_|
                                         
    */

    if (!_.isString(req.param('title'))) {
    return res.badRequest();
    }

    if (!_.isString(req.param('description'))) {
      return res.badRequest();
    }
     if (!_.isString(req.param('imgSrc'))) {
      return res.badRequest();
    }
    
    
    /*if (!_.isNumber(req.param('price'))) {
      return res.badRequest();
    }
     if (!_.isNumber(req.param('height'))) {
      return res.badRequest();
    }
     if (!_.isNumber(req.param('width'))) {
      return res.badRequest();
    }*/
    
    console.log(req.param('price'));
    console.log(req.param('height'));
        console.log(req.param('width'));
    console.log(!_.isNumber(+req.param('price')));
        
    // Find the user that's adding a paint
    User.findOne({
    id: req.session.userId
    }).exec(function(err, foundUser){
      if (err) return res.negotiate;
      if (!foundUser) return res.notFound();
             console.log("find");

      Paint.create({
        title: req.param('title'),
        description: req.param('description'),
        owner: foundUser.id,
        imgSrc:  req.param('imgSrc'),
        price: +req.param('price'),
        height: +req.param('height'),
        width: +req.param('width')
        
        
      })
      .exec(function(err, createdPaint){
        if (err) return res.negotiate(err);
  
             console.log(createdPaint.id);
        return res.json({id: createdPaint.id});
      });
    });
  },

  updatePaint: function(req, res) {

    // Validate parameters
    if (!_.isString(req.param('title'))) {
      return res.badRequest();
    }

    if (!_.isString(req.param('description'))) {
      return res.badRequest();
    } 
    if (!_.isString(req.param('imgSrc'))) {
      return res.badRequest();
    }
    
        if (!_.isNumber(req.param('price'))) {
      return res.badRequest();
    }
     if (!_.isNumber(req.param('height'))) {
      return res.badRequest();
    }
     if (!_.isNumber(req.param('width'))) {
      return res.badRequest();
    }

    // Update the paint coercing the incoming id from a string to an integer using the unary `+` 
    Paint.update({
      id: +req.param('id')
    }, {
      title: req.param('title'),
      description: req.param('description'),
      imgSrc: req.param('imgSrc'),
      price: req.param('price'),
      height:req.param('height'),
      width: req.param('width')
        
    }).exec(function (err) {
      if (err) return res.negotiate(err);

      return res.ok();
    });
  },
  
  deletePaint: function(req, res) {

    // Find the currently logged in user and her paints
    User.findOne({
      id: req.session.userId
    }).exec(function (err, foundUser){
      if (err) return res.negotiate(err);
      if (!foundUser) return res.notFound();

      Paint.findOne({
        id: +req.param('id')
      })
      .populate('owner')
      .populate ('ratings')
      .exec(function(err, foundPaint){
        if (err) return res.negotiate(err);
        if (!foundPaint) return res.notFound();

        // Check ownership
        if (foundUser.id != foundPaint.owner.id) {
          return res.forbidden();
        }
        
        // Destroy the paint
        Paint.destroy({
          id: req.param('id')
        }).exec(function(err){
          if (err) return res.negotiate(err);

         
        });
      });
    });
  },
  
 


 
  
  //Review
  joinReview: function (req, res) {

    // Nothing except socket requests should ever hit this endpoint.
    if (!req.isSocket) {
      return res.badRequest();
    }
    // TODO: ^ pull this into a `isSocketRequest` policy

    // Join the room for this video (as the requesting socket)
    Paint.subscribe(req, req.param('id') );
    
    // Join the the video room for the typing animation
    sails.sockets.join(req, 'paint'+req.param('id'));
   
    return res.ok();
  },

  review: function(req, res) {

    // Nothing except socket requests should ever hit this endpoint.
    if (!req.isSocket) {
      return res.badRequest();
    }
    // TODO: ^ pull this into a `isSocketRequest` policy
    
    Review.create({
      message: req.param('message'),
      sender: req.session.userId,
      paint: +req.param('id')
    }).exec(function (err, createdReview){
      if (err) return res.negotiate(err);

      User.findOne({
        id: req.session.userId
      }).exec(function (err, foundUser){
        if (err) return res.negotiate(err);
        if (!foundUser) return res.notFound();

        // Broadcast WebSocket event to everyone else currently online so their user 
        // agents can update the UI for them.
        // sails.sockets.broadcast('video'+req.param('id'), 'review', {
        //   message: req.param('message'),
        //   username: foundUser.username,
        //   created: 'just now',
        //   gravatarURL: foundUser.gravatarURL
        // });

        // Send a video event to the video record room 
        Paint.publishUpdate(+req.param('id'), {
          message: req.param('message'),
          username: foundUser.username,
          created: 'just now',
          gravatarURL: foundUser.gravatarURL
        });

        return res.ok();
        
      });
    });
  },

  typing: function(req, res) {

    // Nothing except socket requests should ever hit this endpoint.
    if (!req.isSocket) {
      return res.badRequest();
    }
    // TODO: ^ pull this into a `isSocketRequest` policy

    User.findOne({
      id: req.session.userId
    }).exec(function (err, foundUser){
      if (err) return res.negotiate(err);
      if (!foundUser) return res.notFound();

      // Broadcast socket event to everyone else currently online so their user agents
      // can update the UI for them.
      sails.sockets.broadcast('paint'+req.param('id'), 'typing', {
        username: foundUser.username
      }, (req.isSocket ? req : undefined) );

      return res.ok();
    });
  },

  stoppedTyping: function(req, res) {

    // Nothing except socket requests should ever hit this endpoint.
    if (!req.isSocket) {
      return res.badRequest();
    }
    // TODO: ^ pull this into a `isSocketRequest` policy

    // Broadcast socket event to everyone else currently online so their user agents
    // can update the UI for them.
    sails.sockets.broadcast('paint'+req.param('id'),
      'stoppedTyping', {}, (req.isSocket ? req : undefined) );

    return res.ok();
  },
};

