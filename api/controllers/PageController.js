/**
 * PageController
 *
 * @description :: Server-side logic for managing pages
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  home: function(req, res) {

    if (!req.session.userId) {
      return res.view('homepage', {
        me: null
      });
    }

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('homepage', {
          me: null
        });
      }

      return res.view('homepage', {
        me: {
          username: user.username,
          gravatarURL: user.gravatarURL,
          admin: user.admin
        },
        showAddPaintButton: true
      });
    });
  },

  logout: function(req, res) {

    if (!req.session.userId) {
      return res.redirect('/');
    }

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        console.log('error: ', err);
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('homepage');
      }

      return res.view('signout', {
        me: {
          username: user.username,
          gravatarURL: user.gravatarURL,
          admin: user.admin
        }
      });
    });
  },

  editProfile: function(req, res) {

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        console.log('error: ', err);
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('homepage');
      }

      return res.view('edit-profile', {
        me: {
          email: user.email,
          username: user.username,
          gravatarURL: user.gravatarURL,
          admin: user.admin
        }
      });
    });
  },

  profile: function(req, res) {

    User.findOne({
      username: req.param('username')
    })
    .populate("followers")
    .populate("following")
    .exec(function (err, foundUser){
      if (err) return res.negotiate(err);
      if (!foundUser) return res.notFound();

      Paint.find({where: {
        owner: foundUser.id
      }, sort: 'title ASC'})
      .populate('ratings')
      .exec(function (err, foundPaints){
        if (err) return res.negotiate(err);
        if (!foundPaints) return res.notFound();

        /*
          _____                     __                      
         |_   _| __ __ _ _ __  ___ / _| ___  _ __ _ __ ___  
           | || '__/ _` | '_ \/ __| |_ / _ \| '__| '_ ` _ \ 
           | || | | (_| | | | \__ \  _| (_) | |  | | | | | |
           |_||_|  \__,_|_| |_|___/_|  \___/|_|  |_| |_| |_|
                                                  
         */
        
        _.each(foundPaints, function(paint){

          // sync owner
          paint.owner = foundUser.username;

          // Format the createdAt attributes and assign them to the paint
          paint.created = DatetimeService.getTimeAgo({date: paint.createdAt});

          
          var totalSeconds = 0;
        

          // Format average ratings
          var totalRating = 0;
          _.each(paint.ratings, function(rating){
            totalRating = totalRating + rating.stars;
          });

          var averageRating = 0;
          if (paint.ratings.length < 1) {
            averageRating = 0;
          } else {
            averageRating = totalRating / paint.ratings.length;
          }
          
          paint.averageRating = averageRating;
        });

        // The logged out case
        if (!req.session.userId) {
          
          return res.view('profile', {
            // This is for the navigation bar
            me: null,

            // This is for profile body
            username: foundUser.username,
            gravatarURL: foundUser.gravatarURL,
            email: foundUser.email,
            frontEnd: {
              numOfPaints: foundPaints.length,
              numOfFollowers: foundUser.followers.length,
              numOfFollowing: foundUser.following.length
            },
            // This is for the list of paints
            paints: foundPaints
          });
        }

        // Otherwise the user-agent IS logged in.
        // Look up the logged-in user from the database.
        User.findOne({
          id: req.session.userId
        })
        .exec(function (err, loggedInUser){
          if (err) {
            return res.negotiate(err);
          }

          if (!loggedInUser) {
            return res.serverError('User record from logged in user is missing?');
          }

          // Is the logged in user is currently following the owner of this paint?
          var cachedFollower = _.find(foundUser.followers, function(follower){
            return follower.id === loggedInUser.id;
          });

          var followedByLoggedInUser = false;
          if (cachedFollower) {
            followedByLoggedInUser = true;
          }

          // We'll provide `me` as a local to the profile page view.
          // (this is so we can render the logged-in navbar state, etc.)
          var me = {
            username: loggedInUser.username,
            email: loggedInUser.email,
            gravatarURL: loggedInUser.gravatarURL,
            admin: loggedInUser.admin
          };

          // We'll provide the `isMe` flag to the profile page view
          // if the logged-in user is the same as the user whose profile we looked up earlier.
          if (req.session.userId === foundUser.id) {
            me.isMe = true;
          } else {
            me.isMe = false;
          }
          
          // Return me property for the nav and the remaining properties for the profile page.
          return res.view('profile', {
            me: me,
            showAddPaintButton: true,
            username: foundUser.username,
            gravatarURL: foundUser.gravatarURL,
             email: foundUser.email,
            frontEnd: {
              numOfPaints: foundPaints.length,
              numOfFollowers: foundUser.followers.length,
              numOfFollowing: foundUser.following.length,
              followedByLoggedInUser: followedByLoggedInUser
            },
            paints: foundPaints
          });
        }); //</ User.findOne({id: req.session.userId})
      });
    });
  },

  profileFollower: function(req, res) {

    User.findOne({
      username: req.param('username')
    })
    .populate("followers")
    .populate("following")
    .populate("paints")
    .exec(function (err, foundUser){
      if (err) return res.negotiate(err);
      if (!foundUser) return res.notFound();

      // The logged out case
      if (!req.session.userId) {
        
        return res.view('profile-followers', {
          // This is for the navigation bar
          me: null,

          // This is for profile body
          username: foundUser.username,
          gravatarURL: foundUser.gravatarURL,
           email: foundUser.email,
          frontEnd: {
            numOfPaints: foundUser.paints.length,
            numOfFollowers: foundUser.followers.length,
            numOfFollowing: foundUser.following.length,
            followers: foundUser.followers
          },
          // This is for the list of followers
          followers: foundUser.followers
        });
      }

      // Otherwise the user-agent IS logged in.

      // Look up the logged-in user from the database.
      User.findOne({
        id: req.session.userId
      })
      .populate('following')
      .exec(function (err, loggedInUser){
        if (err) {
          return res.negotiate(err);
        }

        if (!loggedInUser) {
          return res.serverError('User record from logged in user is missing?');
        }

        // Is the logged in user currently following the owner of this paint?
        var cachedFollower = _.find(foundUser.followers, function(follower){
          return follower.id === loggedInUser.id;
        });

        // Set the display toggle (followedByLoggedInUser) based upon whether
        // the currently logged in user is following the owner of the paint.
        var followedByLoggedInUser = false;
        if (cachedFollower) {
          followedByLoggedInUser = true;
        }

        // We'll provide `me` as a local to the profile page view.
        // (this is so we can render the logged-in navbar state, etc.)
        var me = {
          username: loggedInUser.username,
          email: loggedInUser.email,
          gravatarURL: loggedInUser.gravatarURL,
          admin: loggedInUser.admin
        };

        // We'll provide the `isMe` flag to the profile page view
        // if the logged-in user is the same as the user whose profile we looked up earlier.
        if (req.session.userId === foundUser.id) {
          me.isMe = true;
        } else {
          me.isMe = false;
        }
        
        // Return me property for the nav and the remaining properties for the profile page.
        return res.view('profile-followers', {
          me: me,
          showAddPaintButton: true,
          username: foundUser.username,
          gravatarURL: foundUser.gravatarURL,
           email: foundUser.email,
          frontEnd: {
            numOfPaints: foundUser.paints.length,
            numOfFollowers: foundUser.followers.length,
            numOfFollowing: foundUser.following.length,
            followedByLoggedInUser: followedByLoggedInUser,
            followers: foundUser.followers
          },
          followers: foundUser.followers
        });
      }); //</ User.findOne({id: req.session.userId})
    });
  },

  profileFollowing: function(req, res) {

    User.findOne({
      username: req.param('username')
    })
    .populate("followers")
    .populate("following")
    .populate("paints")
    .exec(function (err, foundUser){
      if (err) return res.negotiate(err);
      if (!foundUser) return res.notFound();

      // The logged out case
      if (!req.session.userId) {
        
        return res.view('profile-following', {
          // This is for the navigation bar
          me: null,

          // This is for profile body
          username: foundUser.username,
          email: foundUser.email,
          gravatarURL: foundUser.gravatarURL,
          
          frontEnd: {
            numOfPaints: foundUser.paints.length,
            numOfFollowers: foundUser.followers.length,
            numOfFollowing: foundUser.following.length,
            following: foundUser.following
          },
          // This is for the list of following
          following: foundUser.following
        });
      }

      // Otherwise the user-agent IS logged in.

      // Look up the logged-in user from the database.
      User.findOne({
        id: req.session.userId
      })
      .populate('following')
      .exec(function (err, loggedInUser){
        if (err) {
          return res.negotiate(err);
        }

        if (!loggedInUser) {
          return res.serverError('User record from logged in user is missing?');
        }

        // Is the logged in user currently following the owner of this paint?
        var cachedFollower = _.find(foundUser.followers, function(follower){
          return follower.id === loggedInUser.id;
        });

        // Set the display toggle (followedByLoggedInUser) based upon whether
        // the currently logged in user is following the owner of the paint.
        var followedByLoggedInUser = false;
        if (cachedFollower) {
          followedByLoggedInUser = true;
        }

        // We'll provide `me` as a local to the profile page view.
        // (this is so we can render the logged-in navbar state, etc.)
        var me = {
          username: loggedInUser.username,
          email: loggedInUser.email,
          gravatarURL: loggedInUser.gravatarURL,
          admin: loggedInUser.admin
        };

        // We'll provide the `isMe` flag to the profile page view
        // if the logged-in user is the same as the user whose profile we looked up earlier.
        if (req.session.userId === foundUser.id) {
          me.isMe = true;
        } else {
          me.isMe = false;
        }
        
        // Return me property for the nav and the remaining properties for the profile page.
        return res.view('profile-following', {
          me: me,
          showAddPaintButton: true,
          username: foundUser.username,
          email: foundUser.email,
          gravatarURL: foundUser.gravatarURL,
          frontEnd: {
            numOfPaints: foundUser.paints.length,
            numOfFollowers: foundUser.followers.length,
            numOfFollowing: foundUser.following.length,
            followedByLoggedInUser: followedByLoggedInUser,
            following: foundUser.following
          },
          following: foundUser.following
        });
      }); //</ User.findOne({id: req.session.userId})
    });
  },

  signin: function(req, res) {

    return res.view('signin', {
      me: null
    });
  },

  signup: function(req, res) {

    return res.view('signup', {
      me: null
    });
  },

  restoreProfile: function(req, res) {

    return res.view('restore-profile', {
      me: null
    });
  },

  administration: function(req, res) {

    User.findOne(req.session.userId, function(err, user) {

      if (err) {
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('homepage');
      }

      if (!user.admin) {
        return res.redirect('/');
      } else {
        return res.view('administration', {
          me: {
            username: user.username,
            gravatarURL: user.gravatarURL,
            admin: user.admin
          },
          showAddPaintButton: true
        });
      }
    });
  },

  // #1
  passwordRecoveryEmail: function(req, res) {

    return res.view('./password-recovery/password-recovery-email', {
      me: null
    });
  },

  // #2
  passwordRecoveryEmailSent: function(req, res) {

    return res.view('./password-recovery/password-recovery-email-sent', {
      me: null
    });
  },

  // #3
  passwordReset: function(req, res) {

    // Get the passwordRecoveryToken and render the view
    res.view('./password-recovery/password-reset', {
      me: null,
      passwordRecoveryToken: req.param('passwordRecoveryToken')
    });

  },

  showBrowsePage: function(req, res) {

    // If not logged in set `me` property to `null` and pass paints to the view
    if (!req.session.userId) {
      return res.view('browse-paints-list', {
        me: null
      });
    }

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.view('homepage', {
          me: null
        });
      }

      return res.view('browse-paints-list', {
        me: {
          email: user.email,
          gravatarURL: user.gravatarURL,
          username: user.username,
          admin: user.admin
        },
        showAddPaintButton: true
      });
    });
  },
  
  
  paintDetail: function(req, res) {
    // Find the video to play and populate the video `review` association
    Paint.findOne({
      id: req.param('id')
    })
    .populate('owner')
   .populate('reviews')
    .populate('ratings') 
    .exec(function (err, foundPaint){
      if (err) return res.negotiate(err);
      if (!foundPaint) return res.notFound();
      Rating.find({
        byUser: req.session.userId
      }).exec(function(err, foundRating){
        if (err) return res.negotiate(err);

        // If the user agent hasn't made any ratings, assign myRating to null
        if (foundRating.length === 0) {
          foundPaint.myRating = null;
        } else {

          // Iterate through ratings to determine whether the rating matches
          // the id of the paint to be displayed.
          _.each(foundRating, function(rating){

            if (foundPaint.id === rating.byPaint) {
              foundPaint.myRating = rating.stars;
              return;
            }
          });
        }

        // If the paint has no ratings assign averageRating to null.
        if (foundPaint.ratings.length === 0) {
          foundPaint.averageRating = null;
        } else {

          // Calculate the average rating
          // Assign the average to foundPaint.averageRating
          foundPaint.averageRating = MathService.calculateAverage({ratings: foundPaint.ratings});
        }

         
          });
// limit the owner attribute to the users name
        foundPaint.owner = foundPaint.owner.username;

        // Transform createdAt in time ago format
        foundPaint.created = DatetimeService.getTimeAgo({date: foundPaint.createdAt});

        // Transform updatedAt in time ago format
        foundPaint.updated = DatetimeService.getTimeAgo({date: foundPaint.updatedAt});



      //Format each review with the username, gravatarURL, and created date in timeago format
      async.each(foundPaint.reviews, function(review, next){

        User.findOne({
          id: review.sender
        }).exec(function (err, foundUser){
          if (err) return next(err);

          review.username = foundUser.username;
          review.created = DatetimeService.getTimeAgo({date: review.createdAt});
          review.gravatarURL = foundUser.gravatarURL;
          return next();
        });

      }, function(err) {
        if (err) return res.negotiate(err);

        /*
            _____                                      
           |  __ \                                     
           | |__) |___  ___ _ __   ___  _ __  ___  ___ 
           |  _  // _ \/ __| '_ \ / _ \| '_ \/ __|/ _ \
           | | \ \  __/\__ \ |_) | (_) | | | \__ \  __/
           |_|  \_\___||___/ .__/ \___/|_| |_|___/\___|
                           | |                         
                           |_|                                             
        */

        // If not logged in
        if (!req.session.userId) {
          
         return res.view('paints-detail', {
            me: null,
            stars: foundPaint.stars,
            paint: foundPaint,
	      		reviews: foundPaint.reviews
			
          });
        }

        // If logged in...
        User.findOne({
          id: +req.session.userId
        }).exec(function (err, foundUser) {
          if (err) {
            return res.negotiate(err);
          }

          if (!foundUser) {
            sails.log.verbose('Session refers to a user who no longer exists');
            return res.view('paints-detail', {
              me: null,
              reviews: foundPaint.reviews
              
            });
          }

  var me = {
            gravatarURL: foundUser.gravatarURL,
             email: foundUser.email,
            username: foundUser.username,
            admin: foundUser.admin
          };
          
          if (foundUser.username === foundPaint.owner) {
            me.isMe = true;

            return res.view('paints-detail', {
              me: me,
              showAddPaintButton: true,
              stars: foundPaint.stars,
              paint: foundPaint,
            reviews: foundPaint.reviews
            });

          } else {
            return res.view('paints-detail', {
              me: {
                gravatarURL: foundUser.gravatarURL,
                username: foundUser.username,
                admin: foundUser.admin
              },
              showAddPaintButton: true,
              stars: foundPaint.stars,
              paint: foundPaint,
            reviews: foundPaint.reviews
            });
          }
        });
      });
    });
  


  },
  
  newPaint: function(req, res) {

    User.findOne(req.session.userId, function(err, user) {
      if (err) {
        return res.negotiate(err);
      }

      if (!user) {
        sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
        return res.redirect('paints');
      }

      return res.view('paints-detail-new', {
        me: {
          gravatarURL: user.gravatarURL,
          username: user.username,
          admin: user.admin
        }
      });
    });
  },

  editPaint: function(req, res) {

    Paint.findOne({
      id: +req.param('id')
    })
    .populate('owner')
    .exec(function (err, foundPaint){
      if (err) return res.negotiate(err);
      if (!foundPaint) return res.notFound();

      User.findOne({
        id: +req.session.userId
      }).exec(function (err, foundUser) {
        if (err) {
          return res.negotiate(err);
        }

        if (!foundUser) {
          sails.log.verbose('Session refers to a user who no longer exists- did you delete a user, then try to refresh the page with an open tab logged-in as that user?');
          return res.redirect('/paints');
        }

        if (foundUser.username !== foundPaint.owner.username) {
          return res.redirect('/paints/'+foundPaint.id);
        }

        return res.view('paints-detail-edit', {
          me: {
            gravatarURL: foundUser.gravatarURL,
            username: foundUser.username,
            admin: foundUser.admin
          },
          paint: {
            id: foundPaint.id,
            title: foundPaint.title,
            description: foundPaint.description,
            imgSrc:foundPaint.imgSrc,
            price:foundPaint.price,
            width:foundPaint.width,
            height:foundPaint.height
          }
        });
      });
    });
  },

 
 
 
};