module.exports = {
  
  calculateAverage: function (options) {

    var sumPaintRatings = 0;

    _.each(options.ratings, function(rating){
      sumPaintRatings = sumPaintRatings + rating.stars;
    });

    var averageRating = sumPaintRatings / options.ratings.length;

    return averageRating;
  }
};