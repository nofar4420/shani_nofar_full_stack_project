/**
 * Brushfire explicit routes
 *
 */

module.exports.routes = {

  /*************************************************************
  * JSON API ENDPOINTS                                         *
  *************************************************************/

  'PUT /login': 'UserController.login',
  'POST /logout': 'UserController.logout',
  'GET /logout': 'PageController.logout',

  'POST /user/signup': 'UserController.signup',
  'PUT /user/remove-profile': 'UserController.removeProfile',
  'PUT /user/restore-profile': 'UserController.restoreProfile',
  'PUT /user/restore-gravatar-URL': 'UserController.restoreGravatarURL',
  'PUT /user/update-profile': 'UserController.updateProfile',
  'PUT /user/change-password': 'UserController.changePassword',
  'GET /user/admin-users': 'UserController.adminUsers',
  'PUT /user/update-admin/:id': 'UserController.updateAdmin',
  'PUT /user/update-banned/:id': 'UserController.updateBanned',
  'PUT /user/update-deleted/:id': 'UserController.updateDeleted',
  'PUT /user/generate-recovery-email': 'UserController.generateRecoveryEmail',
  'PUT /user/reset-password': 'UserController.resetPassword',
  'PUT /user/follow': 'UserController.follow',
  'PUT /user/unfollow': 'UserController.unFollow',

  'GET /paints': 'PaintController.browsePaints',
  'POST /paints': 'PaintController.createPaint',
  'PUT /paints/:id': 'PaintController.updatePaint',
  'PUT /paints/:id/rate': 'PaintController.ratePaint',

  'POST /paints/:id/review': 'PaintController.review',
  'PUT /paints/:id/join': 'PaintController.joinReview',
  'PUT /paints/:id/typing': 'PaintController.typing',
  'PUT /paints/:id/stoppedTyping': 'PaintController.stoppedTyping',

  'DELETE /paints/:id': 'PaintController.deletePaint',
  
    /*************************************************************
  * Server Rendered HTML Page Endpoints                        *
  *************************************************************/
  
  'GET /profile/followers': 'PageController.profileFollower',

  'GET /': 'PageController.home',
  'GET /profile/edit': 'PageController.editProfile',
  'GET /profile/restore': 'PageController.restoreProfile',
  'GET /signin': 'PageController.signin',
  'GET /signup': 'PageController.signup',
  'GET /administration': 'PageController.administration',

  'GET /password-recovery-email': 'PageController.passwordRecoveryEmail',
  'GET /password-recovery-email-sent': 'PageController.passwordRecoveryEmailSent',  
  'GET /password-reset-form/:passwordRecoveryToken': 'PageController.passwordReset',
  
  'GET /paints/search': 'PaintController.searchPaints',
  'GET /paints/browse': 'PageController.showBrowsePage',
  'GET /paints/new': 'PageController.newPaint',
  'GET /paints/:id': 'PageController.paintDetail',
  'GET /paints/:id/edit': 'PageController.editPaint',
  
  'GET /:username/followers': 'PageController.profileFollower',
  'GET /:username/following': 'PageController.profileFollowing',
  'GET /:username': {
    controller: 'PageController',
    action: 'profile',
    skipAssets: true
  }
  // 'GET /:username': 'PageController.profile',
};