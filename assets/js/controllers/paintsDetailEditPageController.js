angular.module('brushfire').controller('paintsDetailEditPageController', ['$scope', '$http', 'toastr', function($scope, $http, toastr){

/*
   ____          _____                _           
  / __ \        |  __ \              | |          
 | |  | |_ __   | |__) |___ _ __   __| | ___ _ __ 
 | |  | | '_ \  |  _  // _ \ '_ \ / _` |/ _ \ '__|
 | |__| | | | | | | \ \  __/ | | | (_| |  __/ |   
  \____/|_| |_| |_|  \_\___|_| |_|\__,_|\___|_|   
                                                                                                  
*/

  // set-up loading state
  $scope.paintDetailsEdit = {
    loading: false
  };

  // Get the paint id form the current URL path:  /paints/1/edit
  $scope.fromUrlPaintId = window.location.pathname.split('/')[2];

  $scope.me = window.SAILS_LOCALS.me;
  $scope.paint = window.SAILS_LOCALS.paint;

  $scope.paintDetailsEdit.title = $scope.paint.title;
  $scope.paintDetailsEdit.description = $scope.paint.description;
  $scope.paintDetailsEdit.imgSrc = $scope.paint.imgSrc;
  $scope.paintDetailsEdit.price = $scope.paint.price;
  $scope.paintDetailsEdit.width = $scope.paint.width;
  $scope.paintDetailsEdit.height = $scope.paint.height;
/* 
  _____   ____  __  __   ______               _       
 |  __ \ / __ \|  \/  | |  ____|             | |      
 | |  | | |  | | \  / | | |____   _____ _ __ | |_ ___ 
 | |  | | |  | | |\/| | |  __\ \ / / _ \ '_ \| __/ __|
 | |__| | |__| | |  | | | |___\ V /  __/ | | | |_\__ \
 |_____/ \____/|_|  |_| |______\_/ \___|_| |_|\__|___/

*/

  $scope.submitEditPaintForm = function() {

    $http.put('/paints/'+$scope.fromUrlPaintId, {
      title: $scope.paintDetailsEdit.title,
      description: $scope.paintDetailsEdit.description,
      imgSrc:$scope.paintDetailsEdit.imgSrc,
      price:$scope.paintDetailsEdit.price,
      width:$scope.paintDetailsEdit.width,
      height:$scope.paintDetailsEdit.height

    })
    .then(function onSuccess(sailsResponse){
      window.location="/paints/"+$scope.fromUrlPaintId;
    })
    .catch(function onError(sailsResponse){
      console.error(sailsResponse);
    })
    .finally(function eitherWay(){

    });
  };
}]);