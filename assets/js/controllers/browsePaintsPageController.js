angular.module('brushfire').controller('browsePaintsPageController', ['$scope', '$http', 'toastr', function($scope, $http, toastr){

  /*
   ____          _____                _           
  / __ \        |  __ \              | |          
 | |  | |_ __   | |__) |___ _ __   __| | ___ _ __ 
 | |  | | '_ \  |  _  // _ \ '_ \ / _` |/ _ \ '__|
 | |__| | | | | | | \ \  __/ | | | (_| |  __/ |   
  \____/|_| |_| |_|  \_\___|_| |_|\__,_|\___|_|   
                                                                                                    
*/

  $scope.loading = true;
  $scope.results = false;
  $scope.noMorePaints = false;

  // configuration for ui-bootstrap.rating
  $scope.max = 5;
  $scope.isReadonly = true;

  // Pagination properties
  $scope.skip = 0;

  $http({
      url: '/paints',
      method: 'GET',
      params: {
        skip: $scope.skip
      }
    })
    .then(function onSuccess(sailsResponse) {

      $scope.paints = sailsResponse.data.options.updatedPaints;
      $scope.totalPaints = sailsResponse.data.options.totalPaints;

      // Prevent markup from being displayed if no paints
      if ($scope.paints.length > 0) {
        $scope.results = true;
      }

      // Increment the skip variable by the total possible number of paints on the page
      $scope.skip = $scope.skip+=10;

      // Disable the more paints button if there are no more paints.
      if ($scope.paints < 10  || $scope.totalTutrials - $scope.skip <= 10){
        $scope.noMorePaints = true;
      }

    })
    .catch(function onError(sailsResponse) {
      
      // Otherwise, this is some weird unexpected server error. 
      // Or maybe your WIFI just went out.
      console.error('sailsResponse: ', sailsResponse);
    })
    .finally(function eitherWay() {
        $scope.loading = false;
  });

/* 
  _____   ____  __  __   ______               _       
 |  __ \ / __ \|  \/  | |  ____|             | |      
 | |  | | |  | | \  / | | |____   _____ _ __ | |_ ___ 
 | |  | | |  | | |\/| | |  __\ \ / / _ \ '_ \| __/ __|
 | |__| | |__| | |  | | | |___\ V /  __/ | | | |_\__ \
 |_____/ \____/|_|  |_| |______\_/ \___|_| |_|\__|___/

*/

  $scope.fetchMorePaintsLikeThis = function() {
    $scope.loading = true;

    $http({
      url: '/paints',
      method: 'GET',
      params: {
        skip: $scope.skip
      }
    })
    .then(function onSuccess(sailsResponse) {

      // The returned paints
      $scope.paints = sailsResponse.data.options.updatedPaints;

      // The current number of records to skip
      $scope.skip = $scope.skip+=10;

      // Disable the show more paints button when there are no more paints
      if ($scope.skip >= $scope.totalPaints) {
        $scope.noMorePaints = true;
      }

    })
    .catch(function onError(sailsResponse) {
      
      // Otherwise, this is some weird unexpected server error. 
      // Or maybe your WIFI just went out.
      console.error('sailsResponse: ', sailsResponse);
    })
    .finally(function eitherWay() {
        $scope.loading = false;
    });
  };
}]);