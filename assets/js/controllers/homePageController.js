angular.module('brushfire').controller('homePageController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {

/*
   ____          _____                _           
  / __ \        |  __ \              | |          
 | |  | |_ __   | |__) |___ _ __   __| | ___ _ __ 
 | |  | | '_ \  |  _  // _ \ '_ \ / _` |/ _ \ '__|
 | |__| | | | | | | \ \  __/ | | | (_| |  __/ |   
  \____/|_| |_| |_|  \_\___|_| |_|\__,_|\___|_|   
                                                  
                                                  
*/

  // Grab the locals 
  $scope.me = window.SAILS_LOCALS.me;

  
  $scope.loading = false;
  $scope.results = false;
  $scope.noResults = false;
  $scope.noMorePaints = false;

  // configuration for ui-bootstrap.rating
  $scope.max = 5;
  $scope.isReadonly = true;

  // Pagination properties
  $scope.skip = 0;

  SCOPE = $scope;

/* 
  _____   ____  __  __   ______               _       
 |  __ \ / __ \|  \/  | |  ____|             | |      
 | |  | | |  | | \  / | | |____   _____ _ __ | |_ ___ 
 | |  | | |  | | |\/| | |  __\ \ / / _ \ '_ \| __/ __|
 | |__| | |__| | |  | | | |___\ V /  __/ | | | |_\__ \
 |_____/ \____/|_|  |_| |______\_/ \___|_| |_|\__|___/

*/
                                                      
  //
  $scope.searchPaints = function() {
    $scope.loading = true;
    $scope.skip = 0;

    $http({
      url: '/paints/search',
      method: 'GET',
      params: {
        searchCriteria: $scope.searchCriteria,
        skip: $scope.skip
      }
    })
    .then(function onSuccess(sailsResponse) {

      // Search results 
      $scope.paints = sailsResponse.data.options.updatedPaints;

      // Total search results
      $scope.totalPaints = sailsResponse.data.options.totalPaints;

      $scope.results = true;
      // Prevents showing markup with no results
      if ($scope.paints.length > 0) {
        $scope.noResults = false;
        $scope.noMorePaints = false;

      // If on the first pass there are no results show message and hide more results
      } else {
        $scope.noResults = true;
        $scope.noMorePaints = true;
      }

      // if ($scope.paints.length <= 10) {
      //   $scope.noMorePaints = true;
      // }

      $scope.skip = $scope.skip+=10;

    })
    .catch(function onError(sailsResponse) {
      
      // Otherwise, this is some weird unexpected server error. 
      // Or maybe your WIFI just went out.
      console.error('sailsResponse: ', sailsResponse);
    })
    .finally(function eitherWay() {
        $scope.loading = false;
    });
  };

  $scope.fetchMorePaintsLikeThis = function() {
    $scope.loading = true;

    $http({
      url: '/paints/search',
      method: 'GET',
      params: {
        searchCriteria: $scope.searchCriteria,
        skip: $scope.skip
      }
    })
    .then(function onSuccess(sailsResponse) {

      // The returned paints
      $scope.paints = sailsResponse.data.options.updatedPaints;

      // The current number of records to skip
      $scope.skip = $scope.skip+=10;

      // Disable the show more paints button when there are no more paints
      if ($scope.skip >= $scope.totalPaints) {
        $scope.noMorePaints = true;
      }

    })
    .catch(function onError(sailsResponse) {
      
      // Otherwise, this is some weird unexpected server error. 
      // Or maybe your WIFI just went out.
      console.error('sailsResponse: ', sailsResponse);
    })
    .finally(function eitherWay() {
        $scope.loading = false;
    });
  };
}]);