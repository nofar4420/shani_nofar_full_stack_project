angular.module('brushfire').controller('paintsDetailPageController', ['$scope', '$http', 'toastr', function($scope, $http, toastr) {

/*
   ____          _____                _           
  / __ \        |  __ \              | |          
 | |  | |_ __   | |__) |___ _ __   __| | ___ _ __ 
 | |  | | '_ \  |  _  // _ \ '_ \ / _` |/ _ \ '__|
 | |__| | | | | | | \ \  __/ | | | (_| |  __/ |   
  \____/|_| |_| |_|  \_\___|_| |_|\__,_|\___|_|   
                                                                                                    
*/

  // Grab any locals from the me property on the windows object
  $scope.me = window.SAILS_LOCALS.me;

  // Get the paint id form the current URL path:  /paints/1
  $scope.fromUrlPaintId = window.location.pathname.split('/')[2];

  // Grab the number of stars (in a local) for this paint from the stars
  // property of the window object
  $scope.paint = window.SAILS_LOCALS.paint;

  // Expose reviews on the scope so we can render them with ng-repeat.
  $scope.reviews = window.SAILS_LOCALS.reviews;

  // Until we've officially joined the review room, don't allow reviews to be sent.
  $scope.hasJoinedRoom = false;

  // Send a socket request to join the review room.
  io.socket.put('/paints/'+ $scope.fromUrlPaintId + '/join', function (data, JWR) {
    // If something went wrong, handle the error.
    if (JWR.statusCode !== 200) {
      console.error(JWR);
      // TODO
      return;
    }

    // If the server gave us its blessing and indicated that we were
    // able to successfully join the room, then we'll set that on the
    // scope to allow the user to start sending reviews.
    // 
    // Note that, at this point, we'll also be able to start _receiving_ reviews.
    $scope.hasJoinedRoom = true;
    // Because io.socket.get() is not an angular thing, we have to call $scope.$apply()
    // in this callback in order for our changes to the scope to actually take effect.
    $scope.$apply();
  });
  
// Handle socket events that are fired when a new review event is sent (.broadcast)
  io.socket.on('paint', function (e) {

    // Append the review we just received    
    $scope.reviews.push({
      created: e.data.created,
      username: e.data.username,
      message: e.data.message,
      gravatarURL: e.data.gravatarURL
    });

    // Because io.socket.on() is not an angular thing, we have to call $scope.$apply() in
    // this event handler in order for our changes to the scope to actually take effect.
    $scope.$apply();
  });
  
  io.socket.on('typing', function (e) {
    console.log('typing!', e);

    $scope.usernameTyping = e.username;
    $scope.typing = true;

    // Because io.socket.on() is not an angular thing, we have to call $scope.$apply()
    // in this event handler in order for our changes to the scope to actually take effect.
    $scope.$apply();
  });

  io.socket.on('stoppedTyping', function (e) {
    console.log('stoppedTyping!', e);

    $scope.typing = false;

    // Because io.socket.on() is not an angular thing, we have to call $scope.$apply()
    // in this event handler in order for our changes to the scope to actually take effect.
    $scope.$apply();
  });

  
  // set-up loading state
  $scope.paintDetails = {
    loading: false,

    // This is a separate loading state for the delete button
    deletePaintLoading: false
    
  };

  // We need a max for the stars (i.e. 1 out of 5 stars)
  $scope.max = 5;

  // Initial state for the rating directives.
  // Show change rating button initially

  
  // $scope.myRating = 4;
  $scope.myRating = $scope.paint.myRating;
  // $scope.averageRating = 4;
  $scope.averageRating = $scope.paint.averageRating;  // TODO: use window.SAILS_LOCALS... instead of sailsResponse.data.averageStars;

  if ($scope.myRating) {
    $scope.hideChangeRating = false;
    $scope.isReadonly = true;
  } else {
    $scope.hideChangeRating = true;
    $scope.isReadonly = false;
  }


/* 
  _____   ____  __  __   ______               _       
 |  __ \ / __ \|  \/  | |  ____|             | |      
 | |  | | |  | | \  / | | |____   _____ _ __ | |_ ___ 
 | |  | | |  | | |\/| | |  __\ \ / / _ \ '_ \| __/ __|
 | |__| | |__| | |  | | | |___\ V /  __/ | | | |_\__ \
 |_____/ \____/|_|  |_| |______\_/ \___|_| |_|\__|___/

*/

  // When you click the "Change" button for your rating...
  $scope.changeRating = function() {
    // (sets myRating to editable mode)
    $scope.isReadonly = false;
    $scope.hideChangeRating=true;
  };

  // When you hover over "My stars" in edit mode....
  // $scope.hoveringOver = function(rating, paintId) {

  //   // The `id` of the paint, we'll need it for the $watch below because
  //   // we can't pass the `id` in the $watch function
  //   $scope.paintId = paintId;

  //   // The number of stars currently being hovered over
  //   $scope.overStar = rating;
  // };

var origRating = $scope.myRating;
  // When the user changes their rating or sets their initial rating by
  // clicking on the stars in our fancy directive, it changes the `myRating` property
  // on our $scope (which is an ng-model or something) so this watch function fires...
  $scope.$watch('myRating', function(rating) {

    // This disables the rating element between AJAX PUT requests (e.g. double posting)
    if ($scope.paintDetails.loading) {
      return;
    }
    
    if (rating !== origRating) {
      $scope.paintDetails.loading = true;
      $http.put('/paints/' + $scope.fromUrlPaintId + '/rate', {
        stars: rating
      })
      .then(function onSuccess(sailsResponse) {

        toastr.success('Your rating has been saved', 'Rating', {
          closeButton: true
        });

        // Sets myRating to read-only
        $scope.isReadonly = true;
        $scope.hideChangeRating = false;


        // Now, also update the average rating.
        $scope.averageRating = sailsResponse.data.averageRating;

      })
      .catch(function onError(sailsResponse) {
        console.error(sailsResponse);
      })
      .finally(function eitherWay() {
        $scope.paintDetails.loading = false;
      });
    }
  });

  // When you click on a video...
 
  // When you click the "Delete paint" button...
  $scope.deletePaint = function(id) {

    $scope.paintDetails.deletePaintLoading = true;

    $http.delete('/paints/'+id)
    .then(function onSuccess(sailsResponse){

      window.location = "/" + sailsResponse.data.username;

    })
    .catch(function onError(sailsResponse){
      console.error(sailsResponse);
    })
    .finally(function eitherWay(){
      
    });

  };

  // Send review to the review action of the video controller
  $scope.sendMessage = function() {

    io.socket.post('/paints/'+$scope.fromUrlPaintId+'/review', {
      message: $scope.message
    }, function (data, JWR){

      // If something went wrong, handle the error.
      if (JWR.statusCode !== 200) {
        console.error(JWR);
        return;
      }

      // Clear out the review message field.
      // (but rescue its contents first so we can append them)
      var messageWeJustReviewted = $scope.message;
      $scope.message = '';

      $scope.$apply();
    });
  };//</sendMessage>

  $scope.whenTyping = function (event) {

    io.socket.request({
      url: '/paints/'+$scope.fromUrlPaintId+'/typing',
      method: 'put'
    }, function (data, JWR){
        // If something went wrong, handle the error.
        if (JWR.statusCode !== 200) {
          console.error(JWR);
          return;
        }
    });
  };//</whenTyping>

  $scope.whenNotTyping = function (event) {

    console.log('ya')

    io.socket.request({
      url: '/paints/'+$scope.fromUrlPaintId+'/stoppedTyping',
      method: 'put'
    }, function (data, JWR){
        // If something went wrong, handle the error.
        if (JWR.statusCode !== 200) {
          console.error(JWR);
          return;
        }
    });
  };//</whenNotTyping>
}]);