module.exports = {
  email: 'sailsinaction@gmail.com',
  username: 'sailsinaction',
  password: 'abc123',
  admin: true,
  paints: [
    {
      title: 'The Mona Lisa',
      imgSrc:'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ec/Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg/300px-Mona_Lisa%2C_by_Leonardo_da_Vinci%2C_from_C2RMF_retouched.jpg',
      description: 'The classic paint of De Vinci',
      price: 344444444455786,
      height:53,
      width:77,
      ratings: [
        {
          stars: 3,
          user: 'nikolatesla'
        },
        {
          stars: 1,
          user: 'franksinatra'
        }
      ]
    },
    {
      title: 'Luncheon of the Boating Party',
      description: ' painting by French impressionist Pierre-Auguste Renoir. Included in the Seventh Impressionist Exhibition in 1882, it was identified as the best painting in the show by three critics. ',
      imgSrc: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8d/Pierre-Auguste_Renoir_-_Luncheon_of_the_Boating_Party_-_Google_Art_Project.jpg/800px-Pierre-Auguste_Renoir_-_Luncheon_of_the_Boating_Party_-_Google_Art_Project.jpg',
      price: 125000,
      height:130,
      width:172,
     
    },
    {
      title: 'Boulevard Montmartre',
      description: 'Camille Pissarro, Boulevard Montmartre, 1897, the Hermitage, Saint Petersburg',
      imgSrc: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Camille_Pissarro_-_Boulevard_Montmartre_-_Eremitage.jpg/220px-Camille_Pissarro_-_Boulevard_Montmartre_-_Eremitage.jpg',
      price: 1250000
    },
    
  ]
};