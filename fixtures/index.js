/**
 * Given some fixtures, bootstrap the data.
 */

var async = require('async');
var Passwords = require('machinepack-passwords');
var Gravatar = require('machinepack-gravatar');
var FIXTURES = require('./data');

module.exports = function(cb) {

  // Keep a mapping of ratings, these can't be created until all the users have
  // been bootstrapped.
  var RATINGS = {};

  // For each fixture, save the data into the database
  function createUserFromFixture (data, nextUser) {

    async.auto({

      // Create the user record
      createUser: function(next) {
        var userData = _.omit(data, ['paints']);

        async.parallel([
          function encryptPassword(next) {
            Passwords.encryptPassword({
              password: userData.password,
            }).exec(next);
          },
          function gravatar(next) {
            Gravatar.getImageUrl({
              emailAddress: userData.email
            }).exec(next);
          }
        ],

        function(err, results) {
          if(err) return next(err);

          userData.encryptedPassword = results[0];
          userData.gravatarURL = results[1];

          // Create the user
          User.create(userData).exec(next);
        });
      },

      // Create the paint records for the user
      createPaints: ['createUser', function(next, results) {
        var user = results.createUser;
        var paints = data.paints;

        // Store a mapping of paint id's with videos
        var mapping = {};

        function createPaint(paint, nextPaint) {
         var paintData = _.omit(paint, ['ratings']);
          paintData.owner = user.id;

          Paint.create(paintData).exec(function(err, tut) {
            if(err) return nextPaint(err);

            // Record the mapping
         //   mapping[tut.id] = videos;

            // If there are any ratings, map the username and the video id.
            _.each(paint.ratings, function(rating) {
              var ratingsData = RATINGS[rating.user] || [];
              ratingsData.push({ paint: tut.id, stars: rating.stars });
              RATINGS[rating.user] = ratingsData;
            });

            // Add the paint to the user (for use when via-less is being used)
            user.paints.add(tut.id);
            user.save(nextPaint);
          });
        }

        async.eachSeries(paints, createPaint, function(err) {
          if(err) return next(err);
          next(null, mapping);
        });
      }],

      // Create the video records
      
    }, nextUser);

  }

  async.each(FIXTURES, createUserFromFixture, function(err) {
    if(err) return cb(err);

    // Use the ratings mapping now that all the data is created to bootstrap them.
    function bootstrapRatings(username, nextUser) {
      User.findOne({ username: username }).exec(function(err, user) {
        if(err) return nextUser(err);
        if(!user) return nextUser(404);

        async.each(RATINGS[username], function(rating, nextRating) {
          Rating.create({ stars: rating.stars, byUser: user.id, byPaint: rating.paint })
          .exec(nextRating);
        }, nextUser);
      });
    }

    async.each(_.keys(RATINGS), bootstrapRatings, cb);
  });
};